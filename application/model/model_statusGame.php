<?php

function createArraySelectedLetter($selectedLetter){
    if (!in_array($selectedLetter,getValue('selectedLetter'))){  // если нету такой буквы - добавляй
        setValue('selectedLetter',$selectedLetter,true);
    }
    return getValue('selectedLetter');
}

function toggleStateBtn($value,$arraySelectedLetter){
    if (!empty($arraySelectedLetter)){
        if (in_array($value,$arraySelectedLetter)){
            $disabled = "disabled";
        }else{
            $disabled = 'enable';
        }
    }
    return $disabled;
}

function createSplitOnLetters($randomArray){
    foreach ($randomArray as $key => $value) {
        $splitRandomWord = preg_split('//u', $value["word"], null, PREG_SPLIT_NO_EMPTY);
    }
    return $splitRandomWord;
}

function letterCloseOrOpen($arraySelectedLetter,$splitRandomWord){
    if (!empty($splitRandomWord) && !empty($arraySelectedLetter)){
        foreach ($splitRandomWord as $value) {
            if (in_array($value, $arraySelectedLetter)) {
                $unlockedLetter[] = $value;
            } else {
                $unlockedLetter[] = '*';
            }
        }
    }
    return $unlockedLetter;
}

function createOutputSelection($arraySelectedLetter,$splitRandomWord){
    if (!empty($splitRandomWord) && !empty($arraySelectedLetter)) {
        foreach ($arraySelectedLetter as $value) {
            if (count($arraySelectedLetter) == 1){
                $output = "";
            }elseif (in_array($value, $splitRandomWord)) {
                $output = "Есть такая буква";
                $checkUser = true;
            }elseif(!in_array($value, $splitRandomWord)){
                $output = "Вы ошиблись, выберите другую букву";
                $checkUser = false;
            }
        }
    }
    return [$output,$checkUser];
}

function move($checkUser,$output,$selectedLetter){

    if(empty(getValue('countFalseTup'))) {
        setValue('countFalseTup',0);
    }

    if (!$checkUser){
        if (!empty($output)){
            if (!empty($selectedLetter)){
                $_SESSION['countFalseTup'] ++;
                clearPost();
            }
        }
    }
    return getValue('countFalseTup') % MAX_PLAYERS ;
}

function createWinMassage($splitRandomWord,$arraySelectedLetter,$balance){
    if (!empty($splitRandomWord) && !empty($arraySelectedLetter)) {
        foreach ($splitRandomWord as $value) {
            if (in_array($value, $arraySelectedLetter)) {
                $trueValue[] = $value;
                if (count($trueValue) == count($splitRandomWord)) {
                    $winVariable = "Поздравляю,победил - ".getValue('nameUser')[$balance] ;
                }
            }
        }
    }
    return $winVariable;
}
