<?php

function getRandomArray($arrayWords){
    // проверка есть при вызове функции
    $randomWord[] = $arrayWords[array_rand($arrayWords)];
    setValue("randomArray",$randomWord);
    clearPost();
    return getValue('randomArray');
}

function genArrWordWithoutRepeat($arrayWords){
    // сбор слов в корзину.на втором проверит нажатие кнопки , она будет нажата пока не запишется
    if (!empty(getValue('btnStart'))) {
        if (!in_array(getValue('randomArray'), getValue('storageRandomLater'))) {
            setValue('storageRandomLater',getValue('randomArray'),true);
            unset($_SESSION['btnStart']);
        } else {
            return getRandomArray($arrayWords) && genArrWordWithoutRepeat($arrayWords);
        }
    }
}

function saveBtn($nameBtn,$value){
    setValue($nameBtn,$value);
}
