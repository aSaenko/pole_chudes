<?php

function verificationUser(){

    if (!empty($_POST["nameUser"]) && !is_numeric($_POST["nameUser"]) && preg_match("/^[а-яА-ЯёЁa-zA-Z\-_]+$/",$_POST['nameUser']) ){
        return true;
    }else{
        return false;
    }
}

function createArrayName(){
    if (verificationUser()){
        if (!in_array($_POST["nameUser"],getValue('nameUser'))){
            setValue('nameUser',$_POST["nameUser"], true);
        }
        return getValue('nameUser');
    }
}

