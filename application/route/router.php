<?php

if (!$_GET['path']) {
    include 'application/view/naming.php';
    include 'application/view/players.php';
}

if ($_GET['path'] == 'btnStart'){
    include 'application/view/btnStart.php';
    include 'application/view/players.php';
}

if ($_GET['path'] == 'playGame'){
    include 'application/view/question.php';
    include 'application/view/statusGame.php';
    include 'application/view/alphabet.php';
    include 'application/view/players.php';
    if (!empty($winMassage)){
        include 'application/view/winMassage.php';
    }
}
generate404();
