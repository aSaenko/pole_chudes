
<div class="popup__bg">
    <div class="play-again">

        <div class="win">
            <p class="win-massage"><?php echo $winMassage?></p>
        </div>

        <div class="container-btn">

            <div class="box-btn-play-again">
                <form action="" method="post" class="form-play-btn">
                    <input type="submit" name="playAgain_NewPlayers" class="play-again-btn" value="Играть еще раз с новыми игроками">
                </form>
            </div>

            <div class="box-btn-play-again">
                <form action="" method="post" class="form-play-btn">
                    <input type="submit" name="playAgain_OldPlayers" class="play-again-btn" value="Играть еще раз тем же составом">
                </form>
            </div>

        </div>
    </div>
</div>


