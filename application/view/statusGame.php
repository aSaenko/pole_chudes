<?php
include 'application/controller/controller_statusGame.php';
?>

<div class="input-status">
    <p>Сейчас ход - <?php echo getValue('nameUser')[$balance]?></p>

    <div class="wrapper-message">

        <?php if (empty($winMassage)):?>
            <div class="error-or-true">
                <p class="output-result"><?php echo $output?></p>
            </div>
        <?php endif; ?>

    </div>

    <div class="wrapper-result">

        <?php foreach ($arrayToOutput as $value):?>

            <div class="sample-result">
                <p class="hidden-letter"><?php echo $value;?></p>
            </div>

        <?php endforeach; ?>

    </div>
</div>

