

<div class="input-letters">
    <form action="" method="post">

        <?php foreach ($alphabet as $key => $value):
            $disabled = toggleStateBtn($value, $arraySelectedLetter);?>

            <input id="btnAlphabet" type="submit" name="selectedLetter" value="<?php echo $value?>" class="latter" <?php echo $disabled;?>>

        <?php  endforeach;?>

    </form>
</div>
