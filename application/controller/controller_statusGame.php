<?php
include 'application/model/model_statusGame.php';

if (!empty($randomArray)){
    $alphabet = include 'application/data/alphabet.php'; // алфавит
    $selectedLetter = $_POST["selectedLetter"]; //то на что только нажал
    $arraySelectedLetter = createArraySelectedLetter($selectedLetter); // массив нажатых букв , в сессии
    $splitRandomWord = createSplitOnLetters($randomArray);   // разбивает случайное слово на буквы
    $arrayToOutput = letterCloseOrOpen($arraySelectedLetter,$splitRandomWord);  // создает массив для вывода на страницу
    [$output,$checkUser,$checkUserF5] = createOutputSelection($arraySelectedLetter,$splitRandomWord); // выводит ошибку или поздравляет
    $balance = move($checkUser,$output,$selectedLetter); // переход хода (индекс учасника)
    $winMassage = createWinMassage($splitRandomWord,$arraySelectedLetter,$balance); // сообщение об выиграше
}

include 'controller_winMassage.php';
