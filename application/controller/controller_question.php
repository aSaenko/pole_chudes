<?php
include 'application/model/model_question.php';

if (!empty($_POST["choiceWord"]) || !empty(getValue('btnStart'))){
    $arrayWords = include 'application/data/arrayWords.php'; // загадки
    getRandomArray($arrayWords);  //берет случайное слово помещает в сессию
    genArrWordWithoutRepeat($arrayWords); //генерирует массив слов без повтора
}

if (!empty($_POST["choiceWord"])){
    saveBtn('btnStart',$_POST["choiceWord"]); // сохраняет кнопку старта
}