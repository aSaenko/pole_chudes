<?php

if (!empty(genCorrectRandomWord())){
    $randomArray = genCorrectRandomWord(); // коректное случайное слово
}

// функции для того что бы при изменении хранилища данных не нужно было переписавать код
function setValue($name,$value,$flag = false){
    if ($flag){
        $_SESSION[$name][] = $value; // для многомерного сохранения
    }else{
        $_SESSION[$name] = $value;
    }
}

function getValue($name){
    return $_SESSION[$name];
}

function clearPost(){
    header("Location: " . $_SERVER["REQUEST_URI"]); // для обнуления post
}

function enoughUsers(){
    if (!empty(getValue('nameUser')) && count(getValue('nameUser')) == MAX_PLAYERS) {
        return true;
    }
    return false;
}

function genCorrectRandomWord(){
    // проверка корзины, если в корзине есть такое случайное слово
    // создаст сессию с коректным словом и выведет.
    if (in_array(getValue('randomArray'), getValue('storageRandomLater'))) {
        setValue('correctedRandomArray',getValue('randomArray'));
        return getValue('correctedRandomArray');
    }
}

function redirect($url){
    header('Location: '.$url);
}

function generate404(){
    include 'application/data/arrPages.php';
    if (!in_array($_GET['path'],$arrPages)){
        include 'application/view/page404.php';
    }
}